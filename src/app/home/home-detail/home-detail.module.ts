import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeDetailPageRoutingModule } from './home-detail-routing.module';

import { HomeDetailPage } from './home-detail.page';
import { DatePicker } from '@ionic-native/date-picker/ngx';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeDetailPageRoutingModule
  ],
  declarations: [HomeDetailPage],
  providers: [DatePicker]
})
export class HomeDetailPageModule { }
