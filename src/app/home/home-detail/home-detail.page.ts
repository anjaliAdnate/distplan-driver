import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-home-detail',
  templateUrl: './home-detail.page.html',
  styleUrls: ['./home-detail.page.scss'],
})
export class HomeDetailPage implements OnInit {
  homeId: string;
  formTitle: string;
  iconName: string;
  myDate: string;
  myTime: string;
  labor_cost: number = 0.0;
  parts_cost: number = 0.0;
  invoice_num: any;
  comments: string;
  vendor: any;
  Service_type: any;
  odo: number;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private datePicker: DatePicker
  ) {
    var time = new Date();
    this.myDate = time.toLocaleString('default', { month: 'long' }) + ' ' + time.getDate() + ', ' + time.getFullYear();
    this.myTime = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
  
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('homeId')) {
        this.navCtrl.navigateBack('/home');
        return;
      }
      this.homeId = paramMap.get('homeId');
      if (this.homeId === 'issue') {
        this.formTitle = "Add Issue Entry";
        this.iconName = "bug";
      } else if (this.homeId === 'fuel') {
        this.formTitle = "Add Fuel Entry";
        this.iconName = "fuel"
      } else if (this.homeId === 'service') {
        this.formTitle = "Add Service Entry";
        this.iconName = "construct";
      } else if (this.homeId === 'gps') {
        this.formTitle = "GPS Client Config";
        this.iconName = "locate";
      }
    })
  }

  register(form) {
    console.log("form: " + form)
    // this.authService.login(form.value).subscribe((res)=>{
    //   this.router.navigateByUrl('home');
    // });
  }

  showDatepicker() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
      okText: "Save Date",
      todayText: "Set Today"
    }).then(
      date => {
        // this.myDate = date.getDate() + "/" + date.toLocaleString('default', { month: 'long' }) + "/" + date.getFullYear();
        this.myDate = date.toLocaleString('default', { month: 'long' }) + ' ' + date.getDate() + ', ' + date.getFullYear();

      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  showTimepicker() {
    this.datePicker.show({
      date: new Date(),
      mode: 'time',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
      okText: "Save Time",
      nowText: "Set Now"
    }).then(
      time => {
        // this.myTime = time.getHours() + ":" + time.getMinutes();
        this.myTime = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
      },
      err => console.log('Error occurred while getting time: ', err)
    );
  }

}
