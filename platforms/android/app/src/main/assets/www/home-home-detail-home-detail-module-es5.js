function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-detail-home-detail-module"], {
  /***/
  "./node_modules/@ionic-native/date-picker/__ivy_ngcc__/ngx/index.js":
  /*!**************************************************************************!*\
    !*** ./node_modules/@ionic-native/date-picker/__ivy_ngcc__/ngx/index.js ***!
    \**************************************************************************/

  /*! exports provided: DatePicker */

  /***/
  function node_modulesIonicNativeDatePicker__ivy_ngcc__NgxIndexJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DatePicker", function () {
      return DatePicker;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/core */
    "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");

    var DatePicker =
    /** @class */
    function (_super) {
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(DatePicker, _super);

      function DatePicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * @hidden
         */


        _this.ANDROID_THEMES = {
          THEME_TRADITIONAL: 1,
          THEME_HOLO_DARK: 2,
          THEME_HOLO_LIGHT: 3,
          THEME_DEVICE_DEFAULT_DARK: 4,
          THEME_DEVICE_DEFAULT_LIGHT: 5
        };
        return _this;
      }

      DatePicker.prototype.show = function (options) {
        return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "show", {}, arguments);
      };

      DatePicker.pluginName = "DatePicker";
      DatePicker.plugin = "cordova-plugin-datepicker";
      DatePicker.pluginRef = "datePicker";
      DatePicker.repo = "https://github.com/VitaliiBlagodir/cordova-plugin-datepicker";
      DatePicker.platforms = ["Android", "iOS", "Windows"];

      DatePicker.ɵfac = function DatePicker_Factory(t) {
        return ɵDatePicker_BaseFactory(t || DatePicker);
      };

      DatePicker.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
        token: DatePicker,
        factory: function factory(t) {
          return DatePicker.ɵfac(t);
        }
      });

      var ɵDatePicker_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](DatePicker);
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](DatePicker, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"]
        }], null, null);
      })();

      return DatePicker;
    }(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]); //# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvZGF0ZS1waWNrZXIvbmd4L2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sOEJBQXNDLE1BQU0sb0JBQW9CLENBQUM7O0FBQ3hFO0FBR1EsSUFzSndCLDhCQUFpQjtBQUFDO0FBSWpEO0FBR08sUUFOTjtBQUNGO0FBRUEsV0FESztBQUNMLFFBQUUsb0JBQWMsR0FBRztBQUNuQixZQUFJLGlCQUFpQixFQUFFLENBQUM7QUFDeEIsWUFBSSxlQUFlLEVBQUUsQ0FBQztBQUN0QixZQUFJLGdCQUFnQixFQUFFLENBQUM7QUFDdkIsWUFBSSx5QkFBeUIsRUFBRSxDQUFDO0FBQ2hDLFlBQUksMEJBQTBCLEVBQUUsQ0FBQztBQUNqQyxTQUFHLENBQUM7QUFDSjtBQUVlO0FBQU0sSUFLbkIseUJBQUksYUFBQyxPQUEwQjtBQUlsQjtBQUEwQztBQUFxRDtBQUF5QztBQUFzRjtJQXRCaE8sVUFBVSx3QkFEdEIsVUFBVSxFQUFFLFFBQ0EsVUFBVTs7Ozs7MEJBQUU7QUFBQyxxQkEzSjFCO0FBQUUsRUEySjhCLGlCQUFpQjtBQUNoRCxTQURZLFVBQVU7QUFBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIElvbmljTmF0aXZlUGx1Z2luLCBQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIERhdGVQaWNrZXJPcHRpb25zIHtcbiAgLyoqXG4gICAqIFRoZSBtb2RlIG9mIHRoZSBkYXRlIHBpY2tlclxuICAgKiBWYWx1ZXM6IGRhdGUgfCB0aW1lIHwgZGF0ZXRpbWVcbiAgICovXG4gIG1vZGU6IHN0cmluZztcblxuICAvKipcbiAgICogU2VsZWN0ZWQgZGF0ZVxuICAgKi9cbiAgZGF0ZTogRGF0ZSB8IHN0cmluZyB8IG51bWJlcjtcblxuICAvKipcbiAgICogTWluaW11bSBkYXRlXG4gICAqIERlZmF1bHQ6IGVtcHR5IFN0cmluZ1xuICAgKi9cbiAgbWluRGF0ZT86IERhdGUgfCBzdHJpbmcgfCBudW1iZXI7XG5cbiAgLyoqXG4gICAqIE1heGltdW0gZGF0ZVxuICAgKiBEZWZhdWx0OiBlbXB0eSBTdHJpbmdcbiAgICovXG4gIG1heERhdGU/OiBEYXRlIHwgc3RyaW5nIHwgbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBMYWJlbCBmb3IgdGhlIGRpYWxvZyB0aXRsZS4gSWYgZW1wdHksIHVzZXMgYW5kcm9pZCBkZWZhdWx0IChTZXQgZGF0ZS9TZXQgdGltZSkuXG4gICAqIERlZmF1bHQ6IGVtcHR5IFN0cmluZ1xuICAgKi9cbiAgdGl0bGVUZXh0Pzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBMYWJlbCBvZiBCVVRUT05fUE9TSVRJVkUgKGRvbmUgYnV0dG9uKSBvbiBBbmRyb2lkXG4gICAqL1xuICBva1RleHQ/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIExhYmVsIG9mIEJVVFRPTl9ORUdBVElWRSAoY2FuY2VsIGJ1dHRvbikuIElmIGVtcHR5LCB1c2VzIGFuZHJvaWQuUi5zdHJpbmcuY2FuY2VsLlxuICAgKi9cbiAgY2FuY2VsVGV4dD86IHN0cmluZztcblxuICAvKipcbiAgICogTGFiZWwgb2YgdG9kYXkgYnV0dG9uLiBJZiBlbXB0eSwgZG9lc24ndCBzaG93IHRoZSBvcHRpb24gdG8gc2VsZWN0IGN1cnJlbnQgZGF0ZS5cbiAgICovXG4gIHRvZGF5VGV4dD86IHN0cmluZztcblxuICAvKipcbiAgICogTGFiZWwgb2Ygbm93IGJ1dHRvbi4gSWYgZW1wdHksIGRvZXNuJ3Qgc2hvdyB0aGUgb3B0aW9uIHRvIHNlbGVjdCBjdXJyZW50IHRpbWUuXG4gICAqL1xuICBub3dUZXh0Pzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTaG93cyB0aW1lIGRpYWxvZyBpbiAyNCBob3VycyBmb3JtYXQuXG4gICAqL1xuICBpczI0SG91cj86IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIENob29zZSB0aGUgQW5kcm9pZCB0aGVtZSBmb3IgdGhlIHBpY2tlci4gWW91IGNhbiB1c2UgdGhlIERhdGVQaWNrZXIuQU5EUk9JRF9USEVNRVMgcHJvcGVydHkuXG4gICAqIFZhbHVlczogMTogVEhFTUVfVFJBRElUSU9OQUwgfCAyOiBUSEVNRV9IT0xPX0RBUksgfCAzOiBUSEVNRV9IT0xPX0xJR0hUIHwgNDogVEhFTUVfREVWSUNFX0RFRkFVTFRfREFSSyB8IDU6IFRIRU1FX0RFVklDRV9ERUZBVUxUX0xJR0hUXG4gICAqL1xuICBhbmRyb2lkVGhlbWU/OiBudW1iZXI7XG5cbiAgLyoqXG4gICAqIFNob3dzIG9yIGhpZGUgZGF0ZXMgZWFybGllciB0aGVuIHNlbGVjdGVkIGRhdGUuXG4gICAqL1xuICBhbGxvd09sZERhdGVzPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogU2hvd3Mgb3IgaGlkZSBkYXRlcyBhZnRlciBzZWxlY3RlZCBkYXRlLlxuICAgKi9cbiAgYWxsb3dGdXR1cmVEYXRlcz86IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIExhYmVsIG9mIGRvbmUgYnV0dG9uLlxuICAgKi9cbiAgZG9uZUJ1dHRvbkxhYmVsPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBIZXggY29sb3Igb2YgZG9uZSBidXR0b24uXG4gICAqL1xuICBkb25lQnV0dG9uQ29sb3I/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIExhYmVsIG9mIGNhbmNlbCBidXR0b24uXG4gICAqL1xuICBjYW5jZWxCdXR0b25MYWJlbD86IHN0cmluZztcblxuICAvKipcbiAgICogSGV4IGNvbG9yIG9mIGNhbmNlbCBidXR0b24uXG4gICAqL1xuICBjYW5jZWxCdXR0b25Db2xvcj86IHN0cmluZztcblxuICAvKipcbiAgICogWCBwb3NpdGlvbiBvZiBkYXRlIHBpY2tlci4gVGhlIHBvc2l0aW9uIGlzIGFic29sdXRlIHRvIHRoZSByb290IHZpZXcgb2YgdGhlIGFwcGxpY2F0aW9uLlxuICAgKi9cbiAgeD86IG51bWJlcjtcblxuICAvKipcbiAgICogWSBwb3NpdGlvbiBvZiBkYXRlIHBpY2tlci4gVGhlIHBvc2l0aW9uIGlzIGFic29sdXRlIHRvIHRoZSByb290IHZpZXcgb2YgdGhlIGFwcGxpY2F0aW9uLlxuICAgKi9cbiAgeT86IG51bWJlcjtcblxuICAvKipcbiAgICogSW50ZXJ2YWwgYmV0d2VlbiBvcHRpb25zIGluIHRoZSBtaW51dGUgc2VjdGlvbiBvZiB0aGUgZGF0ZSBwaWNrZXIuXG4gICAqL1xuICBtaW51dGVJbnRlcnZhbD86IG51bWJlcjtcblxuICAvKipcbiAgICogRm9yY2UgdGhlIFVJUG9wb3ZlckFycm93RGlyZWN0aW9uIGVudW0uIFRoZSB2YWx1ZSBhbnkgd2lsbCByZXZlcnQgdG8gZGVmYXVsdCBVSVBvcG92ZXJBcnJvd0RpcmVjdGlvbkFueSBhbmQgbGV0IHRoZSBhcHAgY2hvb3NlIHRoZSBwcm9wZXIgZGlyZWN0aW9uIGl0c2VsZi5cbiAgICovXG4gIHBvcG92ZXJBcnJvd0RpcmVjdGlvbj86IHN0cmluZztcblxuICAvKipcbiAgICogRm9yY2UgbG9jYWxlIGZvciBkYXRlUGlja2VyLlxuICAgKi9cbiAgbG9jYWxlPzogc3RyaW5nO1xufVxuXG4vKipcbiAqIEBuYW1lIERhdGUgUGlja2VyXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoZSBEYXRlUGlja2VyIHBsdWdpbiBhbGxvd3MgdGhlIHVzZXIgdG8gZmV0Y2ggZGF0ZSBvciB0aW1lIHVzaW5nIG5hdGl2ZSBkaWFsb2dzLlxuICpcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgRGF0ZVBpY2tlciB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvZGF0ZS1waWNrZXIvbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIGRhdGVQaWNrZXI6IERhdGVQaWNrZXIpIHsgfVxuICpcbiAqXG4gKiAuLi5cbiAqXG4gKlxuICogdGhpcy5kYXRlUGlja2VyLnNob3coe1xuICogICBkYXRlOiBuZXcgRGF0ZSgpLFxuICogICBtb2RlOiAnZGF0ZScsXG4gKiAgIGFuZHJvaWRUaGVtZTogdGhpcy5kYXRlUGlja2VyLkFORFJPSURfVEhFTUVTLlRIRU1FX0hPTE9fREFSS1xuICogfSkudGhlbihcbiAqICAgZGF0ZSA9PiBjb25zb2xlLmxvZygnR290IGRhdGU6ICcsIGRhdGUpLFxuICogICBlcnIgPT4gY29uc29sZS5sb2coJ0Vycm9yIG9jY3VycmVkIHdoaWxlIGdldHRpbmcgZGF0ZTogJywgZXJyKVxuICogKTtcbiAqIGBgYFxuICogQGludGVyZmFjZXNcbiAqIERhdGVQaWNrZXJPcHRpb25zXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnRGF0ZVBpY2tlcicsXG4gIHBsdWdpbjogJ2NvcmRvdmEtcGx1Z2luLWRhdGVwaWNrZXInLFxuICBwbHVnaW5SZWY6ICdkYXRlUGlja2VyJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS9WaXRhbGlpQmxhZ29kaXIvY29yZG92YS1wbHVnaW4tZGF0ZXBpY2tlcicsXG4gIHBsYXRmb3JtczogWydBbmRyb2lkJywgJ2lPUycsICdXaW5kb3dzJ10sXG59KVxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIERhdGVQaWNrZXIgZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG4gIC8qKlxuICAgKiBAaGlkZGVuXG4gICAqL1xuICBBTkRST0lEX1RIRU1FUyA9IHtcbiAgICBUSEVNRV9UUkFESVRJT05BTDogMSxcbiAgICBUSEVNRV9IT0xPX0RBUks6IDIsXG4gICAgVEhFTUVfSE9MT19MSUdIVDogMyxcbiAgICBUSEVNRV9ERVZJQ0VfREVGQVVMVF9EQVJLOiA0LFxuICAgIFRIRU1FX0RFVklDRV9ERUZBVUxUX0xJR0hUOiA1LFxuICB9O1xuXG4gIC8qKlxuICAgKiBTaG93cyB0aGUgZGF0ZSBhbmQvb3IgdGltZSBwaWNrZXIgZGlhbG9nKHMpXG4gICAqIEBwYXJhbSB7RGF0ZVBpY2tlck9wdGlvbnN9IG9wdGlvbnMgT3B0aW9ucyBmb3IgdGhlIGRhdGUgcGlja2VyLlxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxEYXRlPn0gUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIHRoZSBwaWNrZWQgZGF0ZSBhbmQvb3IgdGltZSwgb3IgcmVqZWN0cyB3aXRoIGFuIGVycm9yLlxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBzaG93KG9wdGlvbnM6IERhdGVQaWNrZXJPcHRpb25zKTogUHJvbWlzZTxEYXRlPiB7XG4gICAgcmV0dXJuO1xuICB9XG59XG4iXX0=

    /***/

  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home-detail/home-detail.page.html":
  /*!**********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home-detail/home-detail.page.html ***!
    \**********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomeDetailHomeDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-row sticky=\"tue\" style=\"padding-bottom: 20px; border-bottom: 1px solid #4287f5;\">\n      <ion-col>\n        <ion-row>\n          <ion-col size=\"2\">\n            <ion-icon *ngIf=\"iconName !== 'fuel'\" [name]=\"iconName\"\n              style=\"font-size: 2.3em; color: #fc9536; float: right;\">\n            </ion-icon>\n            <ion-icon *ngIf=\"iconName === 'fuel'\" src=\"assets/{{iconName}}.svg\"\n              style=\"font-size: 2em; color: #fc9536; float: right;\">\n            </ion-icon>\n          </ion-col>\n          <ion-col size=\"8\" style=\"margin: auto;\" class=\"ion-text-center\">\n            <ion-text style=\"font-size: 1.6em; font-weight: 400;\">\n              {{formTitle}}\n            </ion-text>\n          </ion-col>\n          <ion-col class=\"ion-text-center\" style=\"margin: auto;\" size=\"2\" [routerLink]=\"['/', 'home']\">\n            <ion-icon src=\"assets/home.svg\" style=\"font-size: 2.5em;\"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n<ion-content fullscreen>\n  <!-- form will start from here -->\n  <!-- <form> -->\n    <ion-grid>\n      <ion-row color=\"primary\" justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n       \n          <div padding>\n            <ion-item>\n              <ion-label position=\"floating\">Odometer</ion-label>\n              <ion-input type=\"number\" [(ngModel)]=\"odo\" required></ion-input>\n            </ion-item>\n            <p class=\"para\">This has to be greater than 0</p>\n            <ion-item>\n              <ion-label position=\"floating\">Service Type</ion-label>\n              <ion-select placeholder=\"Please choose one\" interface=\"popover\" [(ngModel)]=\"Service_type\" required>\n                <ion-select-option value=\"tire\">Tire Replacement</ion-select-option>\n              </ion-select>\n            </ion-item>\n            <ion-item>\n              <ion-label position=\"floating\">Vendor</ion-label>\n              <ion-select placeholder=\"Please choose one\" interface=\"popover\" [(ngModel)]=\"vendor\" required>\n                <ion-select-option value=\"tire\">No vendor</ion-select-option>\n              </ion-select>\n            </ion-item>\n            <!-- <ion-item> -->\n            <ion-row>\n              <ion-col size=\"12\" style=\"padding: 15px 15px;\">\n                <ion-label style=\"font-size: 0.9em;\">Serviced On</ion-label>\n              </ion-col>\n              <ion-col size=\"6\">\n                <ion-row style=\"padding: 0px 10px; border-bottom: 1px solid lightgray;\">\n                  <ion-col size=\"11\" class=\"ion-no-padding\" (click)=\"showDatepicker()\">\n                    <p style=\"font-size: 0.9em;margin: 0px;\"> {{myDate}}</p>\n                  </ion-col>\n                  <ion-col size=\"1\" style=\"margin: auto; text-align: center;\">\n                    <ion-icon style=\"color: gray; font-size: 0.8em;\" name=\"caret-down-outline\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n              <ion-col size=\"6\">\n                <ion-row style=\"padding: 0px 10px; border-bottom: 1px solid lightgray;\">\n                  <ion-col size=\"11\" class=\"ion-no-padding\" (click)=\"showTimepicker()\">\n                    <p style=\"font-size: 0.9em;margin: 0px;\"> {{myTime}}</p>\n                  </ion-col>\n                  <ion-col size=\"1\" style=\"margin: auto; text-align: center;\">\n                    <ion-icon style=\"color: gray; font-size: 0.8em;\" name=\"caret-down-outline\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <!-- </ion-item> -->\n            <ion-row style=\"border-bottom: 1px solid lightgray;\">\n              <ion-col size=\"7\" style=\"padding: 15px 15px;\">\n                <ion-label style=\"font-size: 0.9em;\">Uploaded Files</ion-label>\n              </ion-col>\n              <ion-col size=\"5\">\n                <ion-button style=\"float: right;\" color=\"light\">Add Files...</ion-button>\n              </ion-col>\n              <ion-col size=\"12\" style=\"padding: 0px 15px;\">\n                <p>No file selected...</p>\n              </ion-col>\n            </ion-row>\n            <!-- <ion-item>\n              <ion-label style=\"font-size: 0.9em;\">Uploaded Files</ion-label>\n              <ion-button slot=\"end\" color=\"light\">Add Files...</ion-button>\n              <p>No file selected...</p>\n            </ion-item> -->\n            <p class=\"para\"> Choose from existing document or from camera.</p>\n            <ion-item class=\"itemst\">\n              <ion-label position=\"floating\">Labor Cost</ion-label>\n              <ion-input type=\"number\" [(ngModel)]=\"labor_cost\" required></ion-input>\n            </ion-item>\n            <p class=\"para\">Service labor total cost</p>\n            <ion-item>\n              <ion-label position=\"floating\">Parts Cost</ion-label>\n              <ion-input class=\"ion-float-right\" type=\"number\" [(ngModel)]=\"parts_cost\" required></ion-input>\n            </ion-item>\n            <p class=\"para\">Car parts total cost</p>\n            <ion-item>\n              <ion-label position=\"floating\">Invoice Number</ion-label>\n              <ion-input type=\"text\" [(ngModel)]=\"invoice_num\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label position=\"floating\">Comments</ion-label>\n              <ion-input type=\"text\" [(ngModel)]=\"comments\" required></ion-input>\n            </ion-item>\n          </div>\n          <!-- <div padding>\n            <ion-button size=\"large\" type=\"submit\" [disabled]=\"form.invalid\" expand=\"block\">Register</ion-button>\n          </div> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  <!-- </form> -->\n  <!-- form will end here -->\n\n\n</ion-content>\n<ion-footer style=\"border-top: 1px solid #4287f5;\">\n  <ion-toolbar>\n    <ion-button shape=\"round\" style=\"float: right\" class=\"round\">\n      <ion-icon slot=\"start\" name=\"arrow-forward-sharp\"></ion-icon>\n      Submit\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/home/home-detail/home-detail-routing.module.ts":
  /*!****************************************************************!*\
    !*** ./src/app/home/home-detail/home-detail-routing.module.ts ***!
    \****************************************************************/

  /*! exports provided: HomeDetailPageRoutingModule */

  /***/
  function srcAppHomeHomeDetailHomeDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeDetailPageRoutingModule", function () {
      return HomeDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _home_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./home-detail.page */
    "./src/app/home/home-detail/home-detail.page.ts");

    var routes = [{
      path: '',
      component: _home_detail_page__WEBPACK_IMPORTED_MODULE_3__["HomeDetailPage"]
    }];

    var HomeDetailPageRoutingModule = function HomeDetailPageRoutingModule() {
      _classCallCheck(this, HomeDetailPageRoutingModule);
    };

    HomeDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], HomeDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/home/home-detail/home-detail.module.ts":
  /*!********************************************************!*\
    !*** ./src/app/home/home-detail/home-detail.module.ts ***!
    \********************************************************/

  /*! exports provided: HomeDetailPageModule */

  /***/
  function srcAppHomeHomeDetailHomeDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeDetailPageModule", function () {
      return HomeDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _home_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./home-detail-routing.module */
    "./src/app/home/home-detail/home-detail-routing.module.ts");
    /* harmony import */


    var _home_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home-detail.page */
    "./src/app/home/home-detail/home-detail.page.ts");
    /* harmony import */


    var _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/date-picker/ngx */
    "./node_modules/@ionic-native/date-picker/__ivy_ngcc__/ngx/index.js");

    var HomeDetailPageModule = function HomeDetailPageModule() {
      _classCallCheck(this, HomeDetailPageModule);
    };

    HomeDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeDetailPageRoutingModule"]],
      declarations: [_home_detail_page__WEBPACK_IMPORTED_MODULE_6__["HomeDetailPage"]],
      providers: [_ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_7__["DatePicker"]]
    })], HomeDetailPageModule);
    /***/
  },

  /***/
  "./src/app/home/home-detail/home-detail.page.scss":
  /*!********************************************************!*\
    !*** ./src/app/home/home-detail/home-detail.page.scss ***!
    \********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomeDetailHomeDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".round {\n  --width: 60px;\n  --height: 60px;\n  --vertical-align: middle;\n  --padding-start: 10px;\n  --padding-end: 10px;\n}\n\n.para {\n  margin: 2px 0px 15px 15px;\n  font-size: 0.75em;\n  color: gray;\n}\n\n.native-input.sc-ion-input-md,\n.native-input.sc-ion-input-ios {\n  text-align: right;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZGlzdHBsYW4tZHJpdmVyL3NyYy9hcHAvaG9tZS9ob21lLWRldGFpbC9ob21lLWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS1kZXRhaWwvaG9tZS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVFBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFFQSx3QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNSRjs7QURXQTtFQUNFLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDUkY7O0FEWUU7O0VBRUUsaUJBQUE7QUNUSiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS1kZXRhaWwvaG9tZS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWl0ZW17XG4vLyAgICAgLS1iYWNrZ3JvdW5kOiAjMzg4MGZmO1xuLy8gICAgIC0tY29sb3I6ICNmZmY7XG4vLyB9XG5cbi8vIGlvbi1idXR0b257XG4vLyAgICAgLS1iYWNrZ3JvdW5kOiAjMDYyZjc3O1xuLy8gfVxuLnJvdW5kIHtcbiAgLS13aWR0aDogNjBweDtcbiAgLS1oZWlnaHQ6IDYwcHg7XG4gIC8vICAgLS1ib3JkZXItcmFkaXVzOiA1MCU7XG4gIC0tdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICAtLXBhZGRpbmctZW5kOiAxMHB4O1xufVxuXG4ucGFyYSB7XG4gIG1hcmdpbjogMnB4IDBweCAxNXB4IDE1cHg7XG4gIGZvbnQtc2l6ZTogMC43NWVtO1xuICBjb2xvcjogZ3JheTtcbn1cblxuLy8gLml0ZW1zdHtcbiAgLm5hdGl2ZS1pbnB1dC5zYy1pb24taW5wdXQtbWQsXG4gIC5uYXRpdmUtaW5wdXQuc2MtaW9uLWlucHV0LWlvcyB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIH1cbi8vIH1cbiIsIi5yb3VuZCB7XG4gIC0td2lkdGg6IDYwcHg7XG4gIC0taGVpZ2h0OiA2MHB4O1xuICAtLXZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgLS1wYWRkaW5nLWVuZDogMTBweDtcbn1cblxuLnBhcmEge1xuICBtYXJnaW46IDJweCAwcHggMTVweCAxNXB4O1xuICBmb250LXNpemU6IDAuNzVlbTtcbiAgY29sb3I6IGdyYXk7XG59XG5cbi5uYXRpdmUtaW5wdXQuc2MtaW9uLWlucHV0LW1kLFxuLm5hdGl2ZS1pbnB1dC5zYy1pb24taW5wdXQtaW9zIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/home/home-detail/home-detail.page.ts":
  /*!******************************************************!*\
    !*** ./src/app/home/home-detail/home-detail.page.ts ***!
    \******************************************************/

  /*! exports provided: HomeDetailPage */

  /***/
  function srcAppHomeHomeDetailHomeDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeDetailPage", function () {
      return HomeDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/date-picker/ngx */
    "./node_modules/@ionic-native/date-picker/__ivy_ngcc__/ngx/index.js");

    var HomeDetailPage = /*#__PURE__*/function () {
      function HomeDetailPage(route, navCtrl, datePicker) {
        _classCallCheck(this, HomeDetailPage);

        this.route = route;
        this.navCtrl = navCtrl;
        this.datePicker = datePicker;
        this.labor_cost = 0.0;
        this.parts_cost = 0.0;
        var time = new Date();
        this.myDate = time.toLocaleString('default', {
          month: 'long'
        }) + ' ' + time.getDate() + ', ' + time.getFullYear();
        this.myTime = time.toLocaleString('en-US', {
          hour: 'numeric',
          minute: 'numeric',
          hour12: true
        }); // this.datePicker.show({
        //   date: new Date(),
        //   mode: 'date',
        //   androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        // }).then(
        //   date => {
        //     console.log('Got date: ', date)
        //   },
        //   err => console.log('Error occurred while getting date: ', err)
        // );
      }

      _createClass(HomeDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          this.route.paramMap.subscribe(function (paramMap) {
            if (!paramMap.has('homeId')) {
              _this2.navCtrl.navigateBack('/home');

              return;
            }

            _this2.homeId = paramMap.get('homeId');

            if (_this2.homeId === 'issue') {
              _this2.formTitle = "Add Issue Entry";
              _this2.iconName = "bug";
            } else if (_this2.homeId === 'fuel') {
              _this2.formTitle = "Add Fuel Entry";
              _this2.iconName = "fuel";
            } else if (_this2.homeId === 'service') {
              _this2.formTitle = "Add Service Entry";
              _this2.iconName = "construct";
            } else if (_this2.homeId === 'gps') {
              _this2.formTitle = "GPS Client Config";
              _this2.iconName = "locate";
            }
          });
        }
      }, {
        key: "register",
        value: function register(form) {
          console.log("form: " + form); // this.authService.login(form.value).subscribe((res)=>{
          //   this.router.navigateByUrl('home');
          // });
        }
      }, {
        key: "showDatepicker",
        value: function showDatepicker() {
          var _this3 = this;

          this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
            okText: "Save Date",
            todayText: "Set Today"
          }).then(function (date) {
            // this.myDate = date.getDate() + "/" + date.toLocaleString('default', { month: 'long' }) + "/" + date.getFullYear();
            _this3.myDate = date.toLocaleString('default', {
              month: 'long'
            }) + ' ' + date.getDate() + ', ' + date.getFullYear();
          }, function (err) {
            return console.log('Error occurred while getting date: ', err);
          });
        }
      }, {
        key: "showTimepicker",
        value: function showTimepicker() {
          var _this4 = this;

          this.datePicker.show({
            date: new Date(),
            mode: 'time',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
            okText: "Save Time",
            nowText: "Set Now"
          }).then(function (time) {
            // this.myTime = time.getHours() + ":" + time.getMinutes();
            _this4.myTime = time.toLocaleString('en-US', {
              hour: 'numeric',
              minute: 'numeric',
              hour12: true
            });
          }, function (err) {
            return console.log('Error occurred while getting time: ', err);
          });
        }
      }]);

      return HomeDetailPage;
    }();

    HomeDetailPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_4__["DatePicker"]
      }];
    };

    HomeDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home-detail',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./home-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home-detail/home-detail.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./home-detail.page.scss */
      "./src/app/home/home-detail/home-detail.page.scss"))["default"]]
    })], HomeDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=home-home-detail-home-detail-module-es5.js.map