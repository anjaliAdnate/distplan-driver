function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Home</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-row>\n          <ion-col class=\"ion-text-center\" style=\"padding-top: 25px;\">\n            <ion-text style=\"font-size: 1.8em; font-weight: 400;\">Driver Entries</ion-text>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col style=\"padding: 25px 8px 15px 8px;\" [routerLink]=\"['/', 'home', 'issue']\">\n            <ion-row>\n              <ion-col size=\"12\" style=\"background: #fbb97c;border-radius: 15px;\">\n                <ion-row>\n                  <ion-col size=\"2\" class=\"ion-text-center\" style=\"margin: auto;\">\n                    <ion-icon style=\"font-size: 2.5em; color: #735539;\" name=\"bug\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"8\" class=\"ion-no-padding\">\n                    <ion-row>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px; font-size: 1.4em; color: #735539; font-weight: bold;\">Add Issue</p>\n                      </ion-col>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px;font-size: 1.1em;\">General issue reports.</p>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <ion-icon name=\"arrow-forward-circle\" color=\"tertiary\" style=\"font-size: 3em;\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col style=\"padding: 0px 8px 15px 8px;\" [routerLink]=\"['/', 'home', 'service']\">\n            <ion-row>\n              <ion-col size=\"12\" style=\"background: #f79383;border-radius: 15px;\">\n                <ion-row>\n                  <ion-col size=\"2\" class=\"ion-text-center\" style=\"margin: auto;\">\n                    <ion-icon style=\"font-size: 2.5em; color: #72433d;\" name=\"construct\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"8\" class=\"ion-no-padding\">\n                    <ion-row>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px; font-size: 1.4em; color: #72433d; font-weight: bold;\">Add Service Entry</p>\n                      </ion-col>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px;font-size: 1.1em;\">Diagnose and repairs entries.</p>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <ion-icon name=\"arrow-forward-circle\" color=\"tertiary\" style=\"font-size: 3em;\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col style=\"padding: 0px 8px 15px 8px;\" [routerLink]=\"['/', 'home', 'fuel']\">\n            <ion-row>\n              <ion-col size=\"12\" style=\"background: #e9cbcb;border-radius: 15px;\">\n                <ion-row>\n                  <ion-col size=\"2\" class=\"ion-text-center\" style=\"margin: auto;\">\n                    <ion-icon style=\"font-size: 2.3em;color: #6b5d5d;\" src=\"assets/fuel.svg\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"8\" class=\"ion-no-padding\">\n                    <ion-row>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px; font-size: 1.4em;color: #6b5d5d; font-weight: bold;\">Add Fuel-Entry</p>\n                      </ion-col>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px;font-size: 1.1em;\">Fuel entry reports.</p>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <ion-icon name=\"arrow-forward-circle\" color=\"tertiary\" style=\"font-size: 3em;\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\" style=\"padding-top: 25px;\">\n            <ion-text style=\"font-size: 1.8em;\">GPS Client Settings</ion-text>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col style=\"padding: 25px 8px 15px 8px;\" [routerLink]=\"['/', 'home', 'gps']\">\n            <ion-row>\n              <ion-col size=\"12\" style=\"background: #ffeddf;border-radius: 15px;\">\n                <ion-row>\n                  <ion-col size=\"2\" class=\"ion-text-center\" style=\"margin: auto;\">\n                    <ion-icon name=\"locate\" style=\"font-size: 2.5em; color: #fc9536;\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"8\" class=\"ion-no-padding\">\n                    <ion-row>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px; font-size: 1.4em; color: #fc9536; font-weight: bold;\">GPS Client Config</p>\n                      </ion-col>\n                      <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <p style=\"margin: 2px;font-size: 1.1em;\">Location tracking settings.</p>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n                  <ion-col size=\"2\">\n                    <ion-icon name=\"arrow-forward-circle\" color=\"tertiary\" style=\"font-size: 3em;\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <div id=\"container\">\n    <h1 style=\"font-family: sans-serif; margin-top: 110%;font-size: 26px; margin-bottom: 8%\">Driver Entries</h1>\n    <ion-grid>\n      <ion-row class=\"outer\" style=\"background: #fbb97c\">\n        <ion-col size=\"2\" style=\"font-size: xx-large; margin-top: 7px;\">\n          <ion-icon name=\"bug-outline\" color=\"dark\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"8\" style=\"text-align: start;\">\n          <ion-label>\n            <h2 style=\"font-size: 20px;\">Add Issue</h2>\n            <h3>General issue reports.</h3>\n          </ion-label>\n          <p></p>\n        </ion-col>\n        <ion-col size=\"2\">\n          <div class=\"three\">\n            <ion-icon name=\"arrow-forward-outline\" style=\"font-size: xx-large; margin-top: 7px; color: white;\">\n            </ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row class=\"outer\" style=\"background: #f79383\">\n        <ion-col size=\"2\" style=\"font-size: xx-large; margin-top: 7px;\">\n          <ion-icon name=\"construct-outline\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"8\" style=\"text-align: start;\">\n          <ion-label>\n            <h2 style=\"font-size: 20px;\">Add Service Entry</h2>\n            <h3>Diagnose and repairs entries.</h3>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"2\">\n          <div class=\"three\">\n            <ion-icon name=\"arrow-forward-outline\" style=\"font-size: xx-large; margin-top: 7px; color: white;\">\n            </ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row class=\"outer\" style=\"background: #e9cbcb\">\n        <ion-col size=\"2\" style=\"font-size: xx-large; margin-top: 7px;\">\n          <ion-icon name=\"battery-full-outline\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"8\" style=\"text-align: start;\">\n          <ion-label>\n            <h2 style=\"font-size: 20px;\">Add Fuel Entry</h2>\n            <h3>Fuel Entry Reports.</h3>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"2\">\n          <div class=\"three\">\n            <ion-icon name=\"arrow-forward-outline\" style=\"font-size: xx-large; margin-top: 7px; color: white;\">\n            </ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <h1 style=\"margin-top: 40px;font-size: 26px;\">\n      GPS Client Settings\n    </h1>\n\n    <ion-grid>\n      <ion-row class=\"outer\" style=\"background: #ffeddf; margin-top: 5%;\">\n        <ion-col size=\"2\" style=\"font-size: xx-large; margin-top: 7px;\">\n          <ion-icon name=\"locate-outline\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"8\" style=\"text-align: start;\">\n          <ion-label>\n            <h2 style=\"font-size: 20px;\">GPS Client Config</h2>\n            <h3>Location tracking Settings</h3>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"2\">\n          <div class=\"three\">\n            <ion-icon name=\"arrow-forward-outline\" style=\"font-size: xx-large; margin-top: 7px; color: white;\">\n            </ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n  </div> -->\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/home/home-routing.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/home/home-routing.module.ts ***!
    \*********************************************/

  /*! exports provided: HomePageRoutingModule */

  /***/
  function srcAppHomeHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function () {
      return HomePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");

    var routes = [{
      path: '',
      component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }];

    var HomePageRoutingModule = function HomePageRoutingModule() {
      _classCallCheck(this, HomePageRoutingModule);
    };

    HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], HomePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/home/home.module.ts":
  /*!*************************************!*\
    !*** ./src/app/home/home.module.ts ***!
    \*************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./home-routing.module */
    "./src/app/home/home-routing.module.ts");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home/home.page.scss":
  /*!*************************************!*\
    !*** ./src/app/home/home.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 20%;\n  transform: translateY(-50%);\n  margin: 10px;\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n.outer {\n  border-radius: 10px;\n  height: 60px;\n}\n\n.three {\n  background: #5260ff;\n  border-radius: 70px;\n  height: 100%;\n  width: 100%;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvZGlzdHBsYW4tZHJpdmVyL3NyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtFQUNBLFlBQUE7QUNBSjs7QURHRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ0FKOztBREdFO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBRUEsY0FBQTtFQUVBLFNBQUE7QUNGSjs7QURLRTtFQUVFLG1CQUFBO0VBQ0EsWUFBQTtBQ0hKOztBRGVHO0VBQ0MsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDWko7O0FEa0JFO0VBQ0UscUJBQUE7QUNmSiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIHRvcDogMjAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICBtYXJnaW46IDEwcHg7XG4gIH1cbiAgXG4gICNjb250YWluZXIgc3Ryb25nIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDI2cHg7XG4gIH1cbiAgXG4gICNjb250YWluZXIgcCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBcbiAgICBjb2xvcjogIzhjOGM4YztcbiAgXG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIFxuICAub3V0ZXIge1xuICAgIC8vYm9yZGVyOiAxcHggc29saWQ7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gIH1cbiAgXG4gIC8vIC5wb25lIHtcbiAgLy8gICBmb250LXNpemU6IDIwcHg7XG4gIC8vICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIC8vICAgY29sb3I6ICM4YzhjOGM7XG4gIC8vICAgbWFyZ2luOiAwO1xuICAvLyAgIGNvbG9yOiBibGFjaztcbiAgLy8gICBwYWRkaW5nOiAxcHg7XG4gIC8vIH1cbiAgXG4gICAudGhyZWUge1xuICAgIGJhY2tncm91bmQ6ICM1MjYwZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCVcbiAgIH1cbiAgLy8gIC5mb3VyIHtcbiAgLy8gICAgYmFja2dyb3VuZDogcmVkO1xuICAvLyAgfVxuICBcbiAgI2NvbnRhaW5lciBhIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIH0iLCIjY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDIwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICBtYXJnaW46IDEwcHg7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgY29sb3I6ICM4YzhjOGM7XG4gIG1hcmdpbjogMDtcbn1cblxuLm91dGVyIHtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgaGVpZ2h0OiA2MHB4O1xufVxuXG4udGhyZWUge1xuICBiYWNrZ3JvdW5kOiAjNTI2MGZmO1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4jY29udGFpbmVyIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/home/home.page.ts":
  /*!***********************************!*\
    !*** ./src/app/home/home.page.ts ***!
    \***********************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var HomePage = /*#__PURE__*/function () {
      function HomePage() {
        _classCallCheck(this, HomePage);
      }

      _createClass(HomePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return HomePage;
    }();

    HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home/home.page.scss"))["default"]]
    })], HomePage);
    /***/
  }
}]);
//# sourceMappingURL=home-home-module-es5.js.map